
import numpy as np
from scipy.spatial.distance import cdist

def similarity_matrix_rbf(Zt, Z, rho = 1000):
    
    assert Z.ndim == 2 and Zt.ndim == 2 , "arrays need to be 2D"
    
    
    F = np.exp(-rho*cdist(Zt, Z, metric='euclidean'))
    denom = F.sum(axis=1)
    denom.shape = (denom.shape[0],1)
    F /= np.tile(denom,(1,F.shape[1]))
    
    return F


def genstruct(nstruct, nvar, ngauss, random_seed = 2394875):
    
    '''
    Function to generate structures such as loadings P using a mixture of gaussians
    
    Parameters:
    -----------
    
    nstruct: number of loading vectors
    nvar: number of spectral variables
    ngauss: number of gaussians for the mixture
    random_seed: int. Seed to use in the simulation
    
    Returns:
    --------
    
    structures: float array of shape (nstruct,nvar)
    
    '''

    structures = np.zeros((nstruct,nvar))

    z = np.arange(nvar)

    for a in range(nstruct):
        
        rng = np.random.RandomState(random_seed*a)
       
        x = rng.choice(nvar, ngauss, replace = True)    
        sd = rng.choice(np.arange(10,200), ngauss, replace = True)
        vec = np.zeros(nvar)

        for i in range(ngauss):

            gaussdens = (1/(np.sqrt(2*np.pi)*sd[i]))*np.exp(-(0.5/np.power(sd[i],2))*np.power(z-x[i],2))
            vec += gaussdens

        structures[a,:] = vec[:]
        
    structures_svd = np.linalg.svd(structures)
        
    return structures,structures_svd[2][0:nstruct,:]



def covsel(X,Y,nvar, scaleY = False, weights = None):
    
    if Y.ndim == 1:
        Y.shape = (Y.shape[0], 1)
    
    x_mu = np.mean(X, axis=0)
    y_mu = np.mean(Y, axis=0)


    n = X.shape[0]
    k = X.shape[1]
    ky = Y.shape[1]


    # centering x
    X_c = X - x_mu
    Y_c = Y - y_mu
    
    if scaleY:
        Y_c /= Y_c.std(axis=0)

    # ------ 1. covsel

    # total var
    
    if weights is None:
        
        weights = np.ones(n)/n
        
    weights_m = np.diag(weights)
    xsstot = np.sum(np.dot(np.dot(X_c.T, weights_m), X_c))
    ysstot = np.sum(np.dot(np.dot(Y_c.T, weights_m), Y_c))


    yss = np.zeros(nvar)
    xss = np.zeros(nvar)
    selvar = np.zeros(nvar).astype(np.int32)
    notselvar = np.zeros(k) == 0
    allvars = np.arange(k)
    


    for ii in range(nvar):

        Sxy = np.dot(np.dot(X_c.T, weights_m), Y_c)
        z = np.diag(np.dot(Sxy,Sxy.T))[notselvar]
        selvar[ii] = allvars[notselvar][np.argmax(z)]
        notselvar[selvar[ii]] = False
        u = X_c[:,selvar[ii]]
        u.shape = (u.shape[0], 1)
        u_inv = 1/np.dot(np.dot(u.T, weights_m), u)
        P = np.dot(np.dot(np.dot(u,u_inv), u.T), weights_m) # P = u %*% solve(t(u) %*% D %*% u) %*% t(u) %*% D
        X_c = X_c - np.dot(P,X_c)
        Y_c = Y_c - np.dot(P,Y_c) 

        xss[ii] = np.sum(np.dot(np.dot(X_c.T, weights_m), X_c))
        yss[ii] = np.sum(np.dot(np.dot(Y_c.T, weights_m), Y_c))
    
        
    selvar_final = selvar.copy()#np.sort(selvar)    
    cumpvarx = 1 - (xss/xsstot)
    cumpvary = 1 - (yss/ysstot)


    return (selvar,cumpvarx,cumpvary)
    
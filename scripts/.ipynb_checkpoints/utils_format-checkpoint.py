

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mtply


from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import GridSearchCV,cross_val_predict
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import LinearRegression

import functions as func
import caltransfer_local as ct
from sample_selection import sample_selection


def get_mod_performance(xcal_pls, ycal_pls, chosen_lv, Xsou_test, Ysou_test, Xtar_test, Ytar_test, 
                        type_model,x_perf_s,y_perf_s,x_perf_t,y_perf_t, n_splits_cv,property_name,orth_params,save_file=None,
                       fig_width = 15,fig_height = 10):


    my_pls = PLSRegression(n_components = chosen_lv,scale=False)
    my_pls.fit(xcal_pls, ycal_pls)
    x_mean = xcal_pls.mean(axis=0)
    y_mean = ycal_pls.mean(axis=0)

    # deliver model

    B = my_pls.coef_
    beta = y_mean - (x_mean.dot(B))


    # crossval performance

    ycal_cv = cross_val_predict(my_pls, xcal_pls, ycal_pls, cv = n_splits_cv)
    rmsecv = np.sqrt(mean_squared_error(ycal_pls, ycal_cv))
    r2cv = np.power(np.corrcoef(ycal_pls.T, ycal_cv.T)[0,1],2)

    # use model in target domain


    Ysou_test_pred = Xsou_test.dot(B) + beta
    rmsep_s = np.sqrt(mean_squared_error(Ysou_test, Ysou_test_pred))
    r2p_s = np.power(np.corrcoef(Ysou_test.T, Ysou_test_pred.T)[0,1],2)

    Ytar_test_pred = Xtar_test.dot(B) + beta
    rmsep_t = np.sqrt(mean_squared_error(Ytar_test, Ytar_test_pred))
    r2p_t = np.power(np.corrcoef(Ytar_test.T, Ytar_test_pred.T)[0,1],2)


    # model performance


    fig, ax = plt.subplots(1,2, figsize = (fig_width,fig_height))

    cal_title = "{:d} lv \nRMSEP: {:.2f} \n$r^2_p$: {:.2f}".format(chosen_lv, rmsep_s, r2p_s) 


    ax[0].plot(Ysou_test, Ysou_test_pred,'o', c = "#2A9EB3", markersize = 15)
    ax[0].plot([np.amin(Ysou_test),np.amax(Ysou_test)],[np.amin(Ysou_test),np.amax(Ysou_test)], c = "black")
    ax[0].set_xlabel("{} Observed".format(property_name))
    ax[0].set_ylabel("{} Predicted".format(property_name))
    ax[0].text(x = x_perf_s, y = y_perf_s, s=cal_title)
    ax[0].set_title("{} model on source".format(type_model))



    test_title = "{:d} lv \nRMSEP: {:.2f} \n$r^2_p$: {:.2f}".format(chosen_lv, rmsep_t, r2p_t) 


    ax[1].plot(Ytar_test, Ytar_test_pred,'o',c = "#2A9EB3", markersize = 15)
    ax[1].plot([np.amin(Ytar_test),np.amax(Ytar_test)],[np.amin(Ytar_test),np.amax(Ytar_test)], c = "black")
    ax[1].set_xlabel("{} Observed".format(property_name))
    ax[1].set_ylabel("{} Predicted".format(property_name))
    ax[1].text(x = x_perf_t, y = y_perf_t, s=test_title)
    ax[1].set_title("{} model on target".format(type_model))
    
    if save_file is not None:
        plt.savefig(save_file,bbox_inches="tight")


    plt.show()

    model_performance = {}
    model_performance["Property"] = property_name
    model_performance["Model"] = type_model
    model_performance["Orthogonalization Parameters"] = orth_params
    model_performance["LV"] = chosen_lv
    model_performance["RMSECV"] = "{:.2f}".format(rmsecv)
    model_performance["R2CV"] = "{:.2f}".format(r2cv)
    model_performance["RMSEP (source)"] = "{:.2f}".format(rmsep_s)
    model_performance["R2P (source)"] = "{:.2f}".format(r2p_s)
    model_performance["RMSEP (target)"] = "{:.2f}".format(rmsep_t)
    model_performance["R2P (target)"] = "{:.2f}".format(r2p_t)
    
    
    return model_performance




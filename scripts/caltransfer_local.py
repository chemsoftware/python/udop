# ------------------------------------------------------------------------
#                           caltransfer methods
# by: Valeria Fonseca Diaz
#  ------------------------------------------------------------------------


import numpy as np
import scipy as sp
from scipy.spatial.distance import cdist




def Vsou_sign(Vsou, Vtar, V):
    
    Vsou_sign = np.zeros(Vsou.shape)

    for h in range(Vsou.shape[1]):
    
        if np.sign(V[:,h].dot(Vsou[:,h])) != np.sign(V[:,h].dot(Vtar[:,h])):

            Vsou_sign[:,h] = -1*Vsou[:,h]

        else:

            Vsou_sign[:,h] = Vsou[:,h]
            
    return Vsou_sign





def epo_fit(X_primary, X_secondary,epo_ncp=1):
    
    
    '''
    
    External Parameter Orthogonalization (EPO) based on spectral value decomposition (SVD) of the difference matrix
    Method based on standard samples
    
    Parameters
    ----------    
    X_primary : ndarray
        A 2-D array corresponding to the spectral matrix. Shape (N, K) from primary domain
        
    X_secondary : ndarray
        A 2-D array corresponding to the spectral matrix. Shape (N, K) from secondary domain. X_secondary is expected to be paired with X_primary (standard samples)
        
    epo_ncp : int
        Number of EPO components to remove. 
          
    Returns
    -------    
    out : tuple
        (E,a), where `E` is the orthogonalization matrix and `a` is the offset.
        E : ndarray (K,K)
        a : ndarray (1,K)
        
    Notes
    -----
    `E` comes from `SVD(D)` where `D = X_primary - X_secondary`
    Orthogonalization of matrices as:
        .. math:: X_{pE} = X_p E 
        .. math:: X_{sE} = X_s E 
    If used for a bilinear model, retrain model using `X_{pE}` and `y`. Obtain model 
        .. math:: y = X_{pE} B_e + \beta_e
    No further orthogonalization is needed for future predictions.
  
        
    References
    ----------
    
    M. Zeaiter, J. M. Roger, and V. Bellon-Maurel, “Dynamic orthogonal projection. A new method to maintain the on-line robustness of multivariate calibrations. Application to NIR-based monitoring of wine fermentations,” Chemom. Intell. Lab. Syst., vol. 80, no. 2, pp. 227–235, 2006, doi: 10.1016/j.chemolab.2005.06.011.
    J. M. Roger, F. Chauchard, and V. Bellon-Maurel, “EPO-PLS external parameter orthogonalisation of PLS application to temperature-independent measurement of sugar content of intact fruits,” Chemom. Intell. Lab. Syst., vol. 66, no. 2, pp. 191–204, 2003, doi: 10.1016/S0169-7439(03)00051-0.
    
    Examples
    --------
    >>> import numpy as np
    >>> import pycaltransfer.caltransfer as caltransfer
    >>> from sklearn.cross_decomposition import PLSRegression
    >>> X_primary = np.array([[0., 0., 1.], [1.,0.,0.], [2.,2.,2.], [2.,5.,4.]])
    >>> x_error = np.array([[-0.03774524, -0.00475627,  0.01938877],
    ...       [-0.02925257,  0.1500586 ,  0.01706783],
    ...       [-0.11049506, -0.03469373, -0.03136003],
    ...       [-0.00685062, -0.00367186, -0.07211823]])
    >>> X_secondary = X_primary + x_error
    >>> # Fit EPO
    >>> E,a = caltransfer.epo_fit(X_primary, X_secondary,epo_ncp=1)
    >>> X_primary_e = X_primary.dot(E) + a
    >>> x_mean_e = np.mean(X_primary_e, axis = 0)
    >>> x_mean_e.shape = (1,X_primary_e.shape[1])
    >>> Y = np.array([[0.1], [0.9], [6.2], [11.9]])
    >>> # PLSR after EPO
    >>> pls2 = PLSRegression(n_components=2,scale=False)
    >>> pls2.fit(X_primary_e, Y)
    >>> B_pe = pls2.coef_
    >>> beta_pe = pls2.y_mean_ - (x_mean_e.dot(B_pe)) # Model after EPO y = X_se B_pe + beta_pe; X_se = X_s E +a
    >>> B_s = B_pe
    >>> beta_s = beta_pe 
    >>> print(X_secondary.dot(B_s) + beta_s)
    >>> print(Y)
    [[ 0.47210387]
     [-0.01622479]
     [ 6.95309782]
     [10.92865516]]
    [[ 0.1]
     [ 0.9]
     [ 6.2]
     [11.9]]

    
    '''  
    
    D = X_primary - X_secondary


    U0,S,V0t = np.linalg.svd(D)
    S_matrix = np.zeros((epo_ncp,epo_ncp))
    S_matrix[0:epo_ncp,:][:,0:epo_ncp] = np.diag(S[0:epo_ncp])
    V = V0t[0:epo_ncp].T
    U = U0[:,0:epo_ncp]

    E = np.identity(n=V.shape[0]) - V.dot(V.T)    
    a = np.mean(X_primary,axis=0)
    a.shape = (1,a.shape[0]) # row vector    

    
    return (E, a)



def top(X_tuple,top_ncp=1):
    
    
    '''
    Transfer by orthogonal projection (TOP).
    Method proposed for standardization samples and transfer to multiple devices.
    
    Parameters
    ----------    
    X_tuple : tuple of ndarray's
        Each array is a  2-D array corresponding to the spectral matrix. Shape (N, K) for N transfer or standardization samples
        
 
    top_ncp : int
        Number of TOP components to remove. 
          
    Returns
    -------    
    out : ndarray
        E, where `E` is the orthogonalization matrix.
        E : ndarray (K,K)
        
        
    Notes
    -----
    `E` comes from `SVD(R)` where `R` contains in each row the mean of one instrument.
    Orthogonalization of matrices as:
        .. math:: X_{pE} = X_p E 
        .. math:: X_{sE} = X_s E 
    If used for a bilinear model, retrain model using `X_{pE}` and `y`. Obtain model 
        .. math:: y = X_{pE} B_e + \beta_e
    No further orthogonalization is needed for future predictions.
  
        
    References
    ----------
    
     A. Andrew and T. Fearn, “Transfer by orthogonal projection: Making near-infrared calibrations robust to between-instrument variation,” Chemom. Intell. Lab. Syst., vol. 72, no. 1, pp. 51–56, 2004, doi: 10.1016/j.chemolab.2004.02.004.
    
    Examples
    --------
    
    >>> import numpy as np
    >>> import pycaltransfer.caltransfer as caltransfer
    >>> from sklearn.cross_decomposition import PLSRegression
    >>> X_primary = np.array([[0., 0., 1.], [1.,0.,0.], [2.,2.,2.], [2.,5.,4.]])
    >>> x_error = np.array([[-0.03774524, -0.00475627,  0.01938877],
    ...       [-0.02925257,  0.1500586 ,  0.01706783],
    ...       [-0.11049506, -0.03469373, -0.03136003],
    ...       [-0.00685062, -0.00367186, -0.07211823]])
    >>> X_secondary = X_primary + x_error
    >>> # TOP
    >>> E = caltransfer.top((X_primary, X_secondary),top_ncp=1)
    >>> X_primary_e = X_primary.dot(E) 
    >>> x_mean_e = np.mean(X_primary_e, axis = 0)
    >>> x_mean_e.shape = (1,X_primary_e.shape[1])
    >>> Y = np.array([[0.1], [0.9], [6.2], [11.9]])
    >>> y_mean = np.mean(Y, axis = 0)
    >>> # PLSR after TOP
    >>> pls2 = PLSRegression(n_components=2,scale=False)
    >>> pls2.fit(X_primary_e, Y)
    >>> B_s = pls2.coef_
    >>> beta_s = y_mean - (x_mean_e.dot(B_s)) # Model after TOP y = X_s B_s + beta_s
    >>> print(X_secondary.dot(B_s) + beta_s)
    >>> print(Y)
        [[ 0.38884864]
         [ 3.76654062]
         [ 3.75167162]
         [12.49809575]]
        [[ 0.1]
         [ 0.9]
         [ 6.2]
         [11.9]]

 

    
    '''  
    
    m = len(X_tuple)
    k = X_tuple[0].shape[1]
    
    assert m > top_ncp, "top_ncp must be less than m. m is {:d}".format(m)
    
    R = np.zeros((m,k))
    
    for mi in range(m):
        
        R[mi,:] = X_tuple[mi].mean(axis=0)
    
 
    U0,S,V0t = np.linalg.svd(R)
    S_matrix = np.zeros((top_ncp,top_ncp))
    S_matrix[0:top_ncp,:][:,0:top_ncp] = np.diag(S[0:top_ncp])
    V = V0t[0:top_ncp].T
    U = U0[:,0:top_ncp]

    E = np.identity(n=V.shape[0]) - V.dot(V.T)       

    
    return E



def dop(Xsou, Ysou, Xtar, Ytar, rho, dop_ncomp = 3):
    
    '''
    Dynamic orthogonal projection (DOP) with Gaussian kernel
    Standard-free method based on reference values of a target domain
    
    Parameters
    ----------
    Xsou: numpy array (N,K)
        Spectral calibration data from source domain

    Ysou: numpy array (N,1)
        Reference values for calibration from source domain

    Xtar: numpy array (N_t,K) 
        Spectral data from target domain

    Ytar: numpy array (N_t,1)
        Reference values from target domain
        
    rho: float
        Precision parameter for Gaussian kernel
        
    dop_ncomp : int
        Number of DOP components to remove. 
          
    Returns
    -------    
    out : ndarray
        E, where `E` is the orthogonalization matrix.
        E : ndarray (K,K)
        
        
    Notes
    -----
    `E` comes from `SVD(D)` with `D = Xtar - Xtar_hat` where `Xtar_hat` is the matrix with supervised virtual standard samples
    Orthogonalization of matrices as:
        .. math:: X_{pE} = X_p E 
        .. math:: X_{sE} = X_s E 
    If used for a bilinear model, retrain model using `X_{pE}` and `y`. Obtain model 
        .. math:: y = X_{pE} B_e + \beta_e
    No further orthogonalization is needed for future predictions.
  
        
    References
    ----------
    
    M. Zeaiter, J. M. Roger, and V. Bellon-Maurel, “Dynamic orthogonal projection. A new method to maintain the on-line robustness of multivariate calibrations. Application to NIR-based monitoring of wine fermentations,” Chemom. Intell. Lab. Syst., vol. 80, no. 2, pp. 227–235, 2006, doi: 10.1016/j.chemolab.2005.06.011.


    Examples
    --------
    
    >>> import numpy as np
    >>> import pycaltransfer.caltransfer as caltransfer
    >>> from sklearn.cross_decomposition import PLSRegression
    >>> Xsou = np.array([[0., 0., 1.], [1.,0.,0.], [2.,2.,2.], [2.,5.,4.]])
    >>> x_error = np.array([[-0.03774524, -0.00475627,  0.01938877],
    ...       [-0.02925257,  0.1500586 ,  0.01706783],
    ...       [-0.11049506, -0.03469373, -0.03136003],
    ...       [-0.00685062, -0.00367186, -0.07211823]])
    >>> Xtar = Xsou + x_error
    >>> Ysou = np.array([[0.1], [0.9], [6.2], [11.9]])
    >>> Ytar = np.array([[0.4], [0.7], [6], [13]])
    >>> # DOP
    >>> E = caltransfer.dop(Xsou, Ysou, Xtar, Ytar, rho = 10, dop_ncomp = 1)
    >>> Xsou_e = Xsou.dot(E) 
    >>> x_mean_e = np.mean(Xsou_e, axis = 0)
    >>> x_mean_e.shape = (1,Xsou_e.shape[1])
    >>> y_mean = np.mean(Ysou, axis = 0)
    >>> # PLSR after EPO
    >>> pls2 = PLSRegression(n_components=2,scale=False)
    >>> pls2.fit(Xsou_e, Ysou)
    >>> B_s = pls2.coef_
    >>> beta_s = y_mean - (x_mean_e.dot(B_s)) # Model after TOP y = X_s B_s + beta_s
    >>> print(Xtar.dot(B_s) + beta_s)
    >>> print(Ytar)
        [[ 0.7848724 ]
         [ 0.46202639]
         [ 5.87275856]
         [11.83470517]]
        [[ 0.4]
         [ 0.7]
         [ 6. ]
         [13. ]]

    
    '''
    
    Ysouq = Ysou.copy()
    if Ysouq.ndim < 2:
        Ysouq.shape = (Ysou.shape[0],1)
    
    Ytarq = Ytar.copy()
    if Ytarq.ndim < 2:    
        Ytarq.shape = (Ytar.shape[0],1)
    


    F = np.exp(-rho*cdist(Ytarq, Ysouq, metric='euclidean'))
    denom = F.sum(axis=1)
    denom.shape = (denom.shape[0],1)
    F /= np.tile(denom,(1,F.shape[1]))


    Xtar_hat = F.dot(Xsou)

    E,a = epo_fit(Xtar_hat, Xtar, epo_ncp = dop_ncomp)
    
    return E


def udop(Xsou, Xtar, udop_ncomp = 1, svd_ncomp = 20):
    
    
    '''
    Unsupervised dynamic orthogonal projection (DOP) 
    Standard-free method based on a separate set from a target domain
    
    Parameters
    ----------
    Xsou: numpy array (N,K)
        Spectral calibration data from source domain
  

    Xtar: numpy array (N_t,K) 
        Spectral data from target domain 
        
        
    udop_ncomp : int
        Number of DOP components to remove. 
        
    svd_ncomp: int
        Number of components for SVD
          
    Returns
    -------    
    out : ndarray
        E, where `E` is the orthogonalization matrix.
        E : ndarray (K,K)
        
        
    Notes
    -----
    `E` comes from `SVD(D)` with `D = Xtar - Xtar_hat` where `Xtar_hat` is the matrix with unsupervised virtual standard samples
    Orthogonalization of matrices as:
        .. math:: X_{pE} = X_p E 
        .. math:: X_{sE} = X_s E 
    If used for a bilinear model, retrain model using `X_{pE}` and `y`. Obtain model 
        .. math:: y = X_{pE} B_e + \beta_e
    No further orthogonalization is needed for future predictions.
    
    
    Examples
    --------
    
    >>> import numpy as np
    >>> import pycaltransfer.caltransfer as caltransfer
    >>> from sklearn.cross_decomposition import PLSRegression
    >>> Xsou = np.array([[0., 0., 1.], [1.,0.,0.], [2.,2.,2.], [2.,5.,4.]])
    >>> x_error = np.array([[-0.03774524, -0.00475627,  0.01938877],
    ...       [-0.02925257,  0.1500586 ,  0.01706783],
    ...       [-0.11049506, -0.03469373, -0.03136003],
    ...       [-0.00685062, -0.00367186, -0.07211823]])
    >>> Xtar = Xsou + x_error
    >>> Y = np.array([[0.1], [0.9], [6.2], [11.9]])
    >>> # uDOP
    >>> E = caltransfer.udop(Xsou,  Xtar, udop_ncomp = 1, svd_ncomp = 20)
    >>> Xsou_e = Xsou.dot(E) 
    >>> x_mean_e = np.mean(Xsou_e, axis = 0)
    >>> x_mean_e.shape = (1,Xsou_e.shape[1])
    >>> y_mean = np.mean(Y, axis = 0)
    >>> # PLSR after EPO
    >>> pls2 = PLSRegression(n_components=2,scale=False)
    >>> pls2.fit(Xsou_e, Y)
    >>> B_s = pls2.coef_
    >>> beta_s = y_mean - (x_mean_e.dot(B_s)) # Model after TOP y = X_s B_s + beta_s
    >>> print(Xtar.dot(B_s) + beta_s)
    >>> print(Y)
        [[ 0.04355672]
         [ 1.21365393]
         [ 5.90318456]
         [11.92067293]]
        [[ 0.1]
         [ 0.9]
         [ 6.2]
         [11.9]]


  
    '''
    
    Xconcat = np.concatenate((Xsou, Xtar), axis = 0)


    Us0, Ss0, Vs_t0 = np.linalg.svd(Xsou - Xsou.mean(axis=0))
    Ut0, St0, Vt_t0 = np.linalg.svd(Xtar - Xtar.mean(axis=0))
    U0, S0, V_t0 = np.linalg.svd(Xconcat - Xconcat.mean(axis=0))




    Vs_t = Vs_t0[0:svd_ncomp,:]
    Vsou = Vs_t.T

    Vt_t = Vt_t0[0:svd_ncomp,:]
    Vtar = Vt_t.T
    
    V_t = V_t0[0:svd_ncomp,:]
    V = V_t.T



    Vsou_s = Vsou_sign(Vsou, Vtar, V)


    Xtar_hat = (Xtar - Xtar.mean(axis=0)).dot(Vtar).dot(Vsou_s.T) + Xtar.mean(axis=0)
    

    E,a = epo_fit(Xtar_hat, Xtar, epo_ncp = udop_ncomp)
    
    return E


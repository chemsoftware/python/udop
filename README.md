# Unsupervised Dynamic Orthogonal Projection (uDOP)
## An efficient approach to calibration transfer without standard samples


Implementation of uDOP for orthogonalization, calibration transfer, model robustness

### 1. Model performance

Performance of source model in source and target domain
![demo](assets/performance_original_mango-1.png)

Performance of orthogonalized model with uDOP in source and target domain
![demo](assets/performance_udop_mango-1.png)


### 2. uDOP components alignment

This is the alignment of loadings in both domains achieved by uDOP after the orthogonalization given a number of uDOP components
![demo](assets/mango_loadings_0udop-1.png)
![demo](assets/mango_loadings_1udop-1.png)
![demo](assets/mango_loadings_2udop-1.png)
![demo](assets/mango_loadings_3udop-1.png)



### 3. uDOP components to be orthogonalized

These are the uDOP components that are orthogonalized or filtered to rebuild the model
![demo](assets/loadings_D_mango-1.png)


### 4. Files

In folder scripts the method and the results in jupyter notebooks can be found
- caltransfer_local.py: Contains the functions of the uDOP methods and other methods included in the original publication
- simulation notebook: Notebook with reported results of the simulation dataset
- mango notebook: Notebook with reported results of the mango dataset


### References

Valeria Fonseca Diaz, Jean-Michel Roger, Wouter Saeys. Unsupervised dynamic orthogonal projection. An efficient approach to calibration transfer without standard samples. Analytica Chimica Acta. Volume 1225,2022,340154, https://doi.org/10.1016/j.aca.2022.340154.
